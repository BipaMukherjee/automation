package Com.Test.Bipa.pages;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;

import org.openqa.selenium.WebElement;

import Com.Test.Bipa.runner.BaseClass;



public class HomePage extends BaseClass{
	
	private By SEARCHTEXTBOX= By.cssSelector("#search_query_top");
	private By SEARCHBUTTON= By.cssSelector(".button-search");
	private By PRODUCTIMAGE= By.cssSelector("img[title='Faded Short Sleeve T-shirts']");
	private By ADDTOCARTBUTTON= By.cssSelector("a[title='Add to cart'] span");
	private By PROCEEDTOCHECKOUTBUTTON= By.cssSelector(".button-container .button-medium span");
	private By TANDCCHECKBOX= By.cssSelector("#cgv");
	private By PAYBYCHECKOPTION= By.cssSelector(".cheque span");
	private By CONFIRMORDERBUTTON= By.cssSelector("#cart_navigation span");
	private By SIGNINLINK= By.linkText("Sign in");
	private By EMAILTEXTBOX= By.cssSelector("#email");
	private By PASSWORDTEXTBOX= By.cssSelector("#passwd");
	private By LOGINBUTTON= By.cssSelector("#SubmitLogin .icon-lock.left");

	public void searchFortheProduct(String searchKeyword) {
		
		action.updateElement(SEARCHTEXTBOX, searchKeyword);
		action.clickOnElement(SEARCHBUTTON);
		wait.waitForMilliSeconds(3000);
		
	}
	
	public void addProductToBasket() {
		action.moveToElements(PRODUCTIMAGE);
		wait.waitForMilliSeconds(3000);
		action.clickOnElement(ADDTOCARTBUTTON);
		
	}
	
	public void proceedToCheckOut() {
		
		
		wait.waitForElementPresent(PROCEEDTOCHECKOUTBUTTON);
		
		WebElement element = driver.findElement(PROCEEDTOCHECKOUTBUTTON);   
		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("arguments[0].click();",element);

		wait.waitForElementPresent(By.cssSelector(".cart_navigation.clearfix .button-medium span"));
		WebElement we1=driver.findElement(By.cssSelector(".cart_navigation.clearfix .button-medium span"));
		js.executeScript("arguments[0].click();",we1);
		
		wait.waitForElementPresent(By.cssSelector(".cart_navigation.clearfix .button-medium span"));
		WebElement we2=driver.findElement(By.cssSelector(".cart_navigation.clearfix .button-medium span"));
		js.executeScript("arguments[0].click();",we2);
		
		
	}
	public void selectTandCOption() {
		
		JavascriptExecutor js = (JavascriptExecutor) driver;
		WebElement element = driver.findElement(TANDCCHECKBOX);
		js.executeScript("arguments[0].click();",element);
		
		wait.waitForElementPresent(By.cssSelector("button[name='processCarrier'] span"));
		WebElement element1 = driver.findElement(By.cssSelector("button[name='processCarrier'] span"));
		js.executeScript("arguments[0].click();",element1);
		
		wait.waitForElementPresent(PAYBYCHECKOPTION);
		WebElement element2 = driver.findElement(PAYBYCHECKOPTION);
		js.executeScript("arguments[0].click();",element2);
		
		wait.waitForElementPresent(CONFIRMORDERBUTTON);
		WebElement element3 = driver.findElement(CONFIRMORDERBUTTON);
		js.executeScript("arguments[0].click();",element3);
		wait.waitForElementPresent(By.cssSelector(".alert.alert-success"));
		
	}
	public void login() {
		action.clickOnElement(SIGNINLINK);
		action.updateElement(EMAILTEXTBOX,"bipa.mukherjee88@gmail.com");
		action.updateElement(PASSWORDTEXTBOX,"BipaBipa");
		action.clickOnElement(LOGINBUTTON);
	
		
	}
	public void editPersonalInfo() {
		action.clickOnElement(By.cssSelector(".account span"));
		wait.waitForMilliSeconds(3000);
		JavascriptExecutor js = (JavascriptExecutor) driver;
		WebElement element = driver.findElement(By.cssSelector("a[title='Information'] span"));
		js.executeScript("arguments[0].click();",element);
		
		wait.waitForMilliSeconds(3000);
		action.moveToElements(By.cssSelector("#firstname"));
		
		action.updateElement(By.cssSelector("#firstname"), "Blippi");
		wait.waitForMilliSeconds(3000);
		action.moveToElements(By.cssSelector("#old_passwd"));
		
		action.updateElement(By.cssSelector("#old_passwd"), "BipaBipa");
		
		action.moveToElements(By.cssSelector("#passwd"));
		
		action.updateElement(By.cssSelector("#passwd"), "BipaBipa");
		action.moveToElements(By.cssSelector("#confirmation"));
		
		action.updateElement(By.cssSelector("#confirmation"), "BipaBipa");
		
		
		WebElement element1 = driver.findElement(By.cssSelector("button[name='submitIdentity'] span"));
		js.executeScript("arguments[0].click();",element1);
		wait.waitForMilliSeconds(3000);
		
		
	}

}
