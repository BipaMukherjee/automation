package Com.Test.Bipa.driverUtility;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import Com.Test.Bipa.runner.BaseClass;

public class Wait extends BaseClass{
	
	public void waitForMilliSeconds(int milliSeconds) {
		try {
			Thread.sleep(milliSeconds);
		} catch (InterruptedException e) {
			
			e.printStackTrace();
		}
	}
	
	public void waitForElementPresent(By element) {
		WebElement webElement=driver.findElement(element);
		WebDriverWait wait = new WebDriverWait(driver,15);
		wait.until(ExpectedConditions.visibilityOf(webElement));
	}

}
