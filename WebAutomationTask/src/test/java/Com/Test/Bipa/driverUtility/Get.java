package Com.Test.Bipa.driverUtility;

import org.openqa.selenium.By;

import Com.Test.Bipa.runner.BaseClass;

public class Get extends BaseClass {

	public String getText(By element) {
		return driver.findElement(element).getText();

	}

}