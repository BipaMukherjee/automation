package Com.Test.Bipa.driverUtility;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import Com.Test.Bipa.runner.BaseClass;

public class Action extends BaseClass {

	public void clickOnElement(By element) {
		WebDriverWait driverWait = new WebDriverWait(driver, 10);
	    WebElement webElement = driverWait.until(ExpectedConditions.elementToBeClickable(element));
	    webElement.click();
		
	}

	public void updateElement(By element, String value) {
		driver.findElement(element).clear();
		driver.findElement(element).sendKeys(value);
	}
	
	public void moveToElements(By element) {
		Actions action = new Actions(driver);
		WebElement we = driver.findElement(element);
		action.moveToElement(we).build().perform();
	}

}
