package Com.Test.Bipa.stepDefinations;

import org.junit.Assert;
import org.openqa.selenium.By;

import Com.Test.Bipa.runner.BaseClass;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class PlaceOrderStepDef extends BaseClass{
	
	@Given("^I login with valid credentials$")
	public void i_login_with_valid_credentials() throws Throwable {
		
		homePage.login();
		
	    
	}

	@Given("^I am in personal information page$")
	public void i_am_in_personal_information_page() throws Throwable {
	    
	}

	@When("^I edit my first name$")
	public void i_edit_my_first_name() throws Throwable {
		
		homePage.editPersonalInfo();
	    
	}

	@Then("^I should see the changed to edited name$")
	public void i_should_see_the_changed_to_edited_name() throws Throwable {
		Assert.assertEquals("Your personal information has been successfully updated.", driver.findElement(By.cssSelector(".alert.alert-success")).getText());
		   
	}
	@Given("^I am in Payment page with \"([^\"]*)\"$")
	public void i_am_in_Payment_page_with(String searchKeyword) throws Throwable {
		
		homePage.searchFortheProduct(searchKeyword);
		homePage.addProductToBasket();
		homePage.proceedToCheckOut();
		homePage.selectTandCOption();
		
		
    
	}

	@When("^I proceed payment option as check$")
	public void i_proceed_payment_option_as_check() throws Throwable {
	    
	}

	@Then("^I should see the order confirmation page$")
	public void i_should_see_the_order_confirmation_page() throws Throwable {
		
		Assert.assertEquals("Your order on My Store is complete.", driver.findElement(By.cssSelector(".alert.alert-success")).getText());
		
	}


}
