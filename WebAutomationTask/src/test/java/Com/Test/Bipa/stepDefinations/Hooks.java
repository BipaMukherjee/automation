package Com.Test.Bipa.stepDefinations;

import org.openqa.selenium.By;
import org.openqa.selenium.chrome.ChromeDriver;

import Com.Test.Bipa.runner.BaseClass;

import cucumber.api.java.After;
import cucumber.api.java.Before;

public class Hooks extends BaseClass{
	
	private static String BASE_URL="http://automationpractice.com/";
	private static String driverPath="./src/test/resources/drivers/chromedriver";
	
	
	@Before 
	public void start() throws InterruptedException {
		System.setProperty("webdriver.chrome.driver", driverPath);
		driver=new ChromeDriver();
		driver.get(BASE_URL);
		wait.waitForMilliSeconds(3000);
		
	}
	
	@After
	public void close() {
		//driver.close();
		
	}

}
