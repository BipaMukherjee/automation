package Com.Test.Bipa.runner;

import org.openqa.selenium.WebDriver;

import Com.Test.Bipa.driverUtility.Action;
import Com.Test.Bipa.driverUtility.Get;
import Com.Test.Bipa.driverUtility.Wait;
import Com.Test.Bipa.pages.HomePage;



public class BaseClass {

	public static WebDriver driver;
	 public static HomePage homePage=new HomePage();
	
	public static Action action = new Action();
	public static Get get = new Get();
	public static Wait wait = new Wait();
	

}
