package Com.Test.Bipa.runner;

import org.junit.runner.RunWith;
import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;

@RunWith(Cucumber.class)
@CucumberOptions(
			features = "./src/test/resources/features",
			 glue = { "com.stepDefinations" },
			 tags= {"@TEST1"},
			plugin = { "html:target/cucumber-html-report", "json:target/cucumber.json" }
            )

public class CucumberRunner {

}
